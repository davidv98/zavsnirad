import requests

#SKRIPTA ZA ULAZ NA ADMIN DEO SAJTA
# FUNKCIJA VRACA TOKEN SESIJE 
def admin():
    podaci = {"admin":"irina","pass":"123","login":"Login"}
    odg = requests.post("http://gubitnik.com/admin/",data=podaci)
    token = (odg.headers['Set-Cookie'])
    return(token)

# SKRIPTA ZA PRIKAZ STRANICE ZA ULOGOVANOG KORISNIKA
def ulogovani_korisnik():
    odg = requests.get("http://gubitnik.com/admin/home.php",headers={"Cookie":admin()})
    return odg.text

#LOGOVANJE NA ADMIN BEZ USERNAME I PASSWORDA
def logovanje_bez_parametara():
    podaci = {"admin":"","pass":"","login":"Login"}
    odg = requests.post("http://gubitnik.com/admin/",data=podaci)
    token = odg.headers['Set-Cookie']
    odg = requests.get("http://gubitnik.com/admin/home.php",headers={"Cookie":token})
    return(odg.text)

# SKRIPTA ZA UNOS NOVE KNJIGE 
def unos_knjige():
    for i in range(1,11):
        knjiga = {"product_name":f"Test_naslov{i}","price":"100","isbn":"111","autor":"Test_autor",
        "num_pages":"100","category":"1","body":"<p>Lorem Ipsum is simply Lorem Ipsum is simply Lorem Ipsum is simply Lorem</p>",
        "image":"kult.jpg","go":"Save"}
        unos = requests.post("http://gubitnik.com/admin/home.php",headers={"Cookie":admin()},
        data=knjiga)
    return(unos.text)

# SKRIPTA ZA IZMENU POSTOJECE KNJIGE 
def edit_knjige():
    promena_knjige = {"product_name":"Test_naslov","price":"1000","isbn":"111","autor":"Test_autor",
        "num_pages":"100","category":"1","body":"<p>Lorem Ipsum is simply Lorem Ipsum is simply Lorem Ipsum is simply Lorem</p>",
        "image":"kult.jpg","submit":"Update"}
    edit = requests.get("http://gubitnik.com/admin/view_products.php",headers={"Cookie":admin()})
    for i in ids():
        edit = requests.post(f"http://gubitnik.com/admin/edit_product.php?editpro={i}",data=promena_knjige)
    return(edit.text)

# SKRIPTA ZA BRISANJE POSTOJECE KNJIGE 
def obrisi_knjigu():
    odg = requests.get("http://gubitnik.com/admin/view_products.php",headers={"Cookie":admin()})
    for i in ids():
        odg = requests.delete(f"http://gubitnik.com/admin/view_products.php?delpro={i}")
    return(odg.text)

# VRACANJE ID IZ API.PHP PO USLOVU ZA HTTP METODE
def ids():
    prazna = []
    api = requests.get("http://gubitnik.com/api.php").json()
    for i in api:
        if (int(i['product_id']) > 39):
            prazna.append(i['product_id'])
    return prazna

def api1():
    odg = requests.get("http://gubitnik.com/api.php").json()
    return odg

# DA LI POSTOJI NASLOV ZA SVAKU KNJIGU
def vrati_naslov_api():
    prazna = []
    for i in api1():
        prazna.append(i['product_name']) 
    return prazna
     
#DA LI POSTOJI EKSTENZIJA U NAZIVU SLIKE
def ekstenzija_api():
    prazna = []
    for i in api1():
        prazna.append(i['image']) 
    return prazna

#DA LI SU ID BROJEVI
def id_broj():
    prazna = []
    for i in api1():
        prazna.append(i['product_id'])
    return prazna

#DA LI SU CENE BROJEVI
def cene_api():
    prazna = []
    for i in api1():
        prazna.append(i['price'])
    return prazna