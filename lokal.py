import requests
from bs4 import BeautifulSoup

def podaci_lokal():
    odg = open("podaciLokal.txt","r").readlines()
    odg = [o.strip() for o in odg]
    return(odg)

def kategorije():
    odg = requests.get("http://gubitnik.com/").text
    odg = BeautifulSoup(odg,features="html.parser")
    cat = odg.find_all("a")
    cat = [c['href'] for c in cat if "product_cat.php?cat=" in c['href']]
    c = cat[0:9]
    return c

def product_link():
    prazna = []
    for k in kategorije():
                odg = requests.get(f"http://gubitnik.com/{k}").text
                odg = BeautifulSoup(odg,features="html.parser")
                linkovi = odg.find_all("h5")
                linkovi = [tuple(l)[0]['href'] for l in linkovi]
                for l in linkovi:
                    prazna.append(l)
    return prazna

def server_naslov_cena():
    prazna = []
    for k in kategorije():
            odg = requests.get(f"http://gubitnik.com/{k}").text
            odg = BeautifulSoup(odg,features="html.parser")
            for l in product_link():
                    odgovor = requests.get(f"http://gubitnik.com/{l}").text
                    odgovor = BeautifulSoup(odgovor,features="html.parser")
                    naslov = odgovor.find_all("h1")
                    cena = odgovor.find_all("h2")
                    cena = [tuple(c)[0].text for c in cena][0]
                    naslov_cena = [naslov[1].text + "," + cena]
                    for n in naslov_cena:
                        prazna.append(n)
            return(prazna)