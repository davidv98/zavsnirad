from selenium import webdriver
from selenium.webdriver.chrome import options
from selenium.webdriver.chrome.options import Options

def user():
    username = open("podaci.txt","r").readlines()
    username = [u.split(",") for u in username][0]
    return username

def test_logovanje():
    for i in user():
        chrome_options = Options()
        # chrome_options.add_argument("--headless")
        driver = webdriver.Chrome()
        driver.get("http://gubitnik.com/")
        driver.find_element_by_link_text("My Account").click()
        driver.find_element_by_id("ime").send_keys(f"{i}")
        driver.find_element_by_id("pass").send_keys("123")
        driver.find_element_by_xpath("//div/div/div/div/div/form/input").click()
        try:
            driver.find_element_by_partial_link_text("OK").click()
            
        except:
            driver.find_element_by_partial_link_text("OK").click()
            driver.save_screenshot(f"slika_logovanje_{i}.png")
        log_name = driver.find_element_by_xpath("//div/div/div/ul/li/a[@href='#']").text
        assert "Irina" in log_name or "Jelena" in log_name
        driver.quit()
        


    
